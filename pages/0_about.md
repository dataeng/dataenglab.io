---
layout: page
title: About
permalink: /about/
feature-img: "assets/img/pexels/retro-school.jpeg"
---

About us:

 * [Meet the team]({% link pages/people.md %})
 * [Collaboration]({% link pages/collaboration.md %})
 * [Visit us]({% link pages/location.md %})

