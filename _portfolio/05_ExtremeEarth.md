---
layout: post
title: Extreme Earth
permalink: /projects/xearth/
img: "assets/img/logos/xearth.png"
date: January 2019
tags: [Earth Observation, Semagrow, Big Data]
---

The Extreme Earth action (January 2019 - December 2021) develops
analytics techniques and technologies that scale to the petabytes of
big Copernicus data, information and knowledge, and applies these
technologies in two of the thematic exploitation platforms of the
European Space Agency: the one dedicated to Food Security and the one
dedicated to the Polar regions.

Visit
[http://earthanalytics.eu](http://earthanalytics.eu)
for more details.
