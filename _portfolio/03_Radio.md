---
layout: post
title: Radio
permalink: /projects/radio/
img: "assets/img/logos/radio.png"
date: April 2015
tags: [Health Data]
---

![image]({{ site.baseurl }}/{{ page.img }})

The Radio action (April 2015 - March 2018) paved the way for wider
deployment of technology solutions in active and healthy ageing,
by developing and prototyping architectures and technologies for a
wide-area ecosystem of smart homes featuring robotics and IoT
technologies.

Visit [http://www.radio-project.eu](https://www.radio-project.eu) for
public deliverables and other details.

