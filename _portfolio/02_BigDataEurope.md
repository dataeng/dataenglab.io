---
layout: post
title: Big Data Europe
hide_title: Yes
permalink: /projects/bigdataeurope/
img: "assets/img/logos/bigdataeurope-vertical.png"
date: January 2015
tags: [BigDataEurope, Semagrow, Big Data]
---

![image]({{ site.baseurl }}/assets/img/logos/bigdataeurope-743x133.png)

The Big Data Europe coordination and support action (January 2015 -
December 2017) carried out the foundational work for enabling European
companies to build innovative multilingual products and services based
on semantically interoperable, large-scale, multi-lingual data assets
and knowledge, available under a variety of licenses and business
models.

The action collected requirements for the ICT infrastructure needed by
data-intensive science practitioners tackling a wide range of societal
challenges; covering all aspects of publishing and consuming
semantically interoperable, large-scale, multi-lingual data assets and
knowledge. Based on these, the action designed and implemented an
architecture for an infrastructure that meets requirements, minimizes
the disruption to current workflows, and maximizes the opportunities
to take advantage of the latest European RTD developments, including
multilingual data harvesting, data analytics, and data visualization.

This infrastructure, the Big Data Integrator (BDI) has been piloted on
use cases covering all seven societal challenges set in the Horizon 2020
work programme and has attracted a growing community of users and
contributors beyond the original action's partners.

Visit
[https://github.com/big-data-europe](https://github.com/big-data-europe)
for the BDI platform or
[https://www.big-data-europe.eu](https://www.big-data-europe.eu) for
public deliverables and other details about the Big Data Europe action.
