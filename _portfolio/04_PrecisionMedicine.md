---
layout: post
title: Precision Medicine
permalink: /projects/precmed/
img: "assets/img/logos/pmnet.png"
date: May 2018
tags: [Bioinformatics, BigDataEurope, Big Data, Health Data]
---

The mession of the Hellenic Network of Precision Medicine on Cancer
(May 2018 - August 2020) is to enrich diagnosis knowledge and
prediction outcome; improve the targeted therapeutical treatment of
cancer patients; connect the Network partners with the National Health
System; and provide high-quality health services.

Visit
[https://oncopmnet.gr](https://oncopmnet.gr/?page_id=2921&lang=en) for
more details.
