---
layout: post
title: Nefeli Prokopaki Kostopoulou
permalink: /about/people/nefelipk.html
img: assets/faces/nefelipk.jpeg
---

![photo](/assets/faces/nefelipk.jpeg)

Nefeli Prokopaki Kostopoulou holds a BSc in Informatics and
Telecommunications (University of Athens, 2016), and an MSc in
Computing Systems (University of Athens, 2019). Her main research
interests are computational logic, artificial intelligence and
protection and security of information systems.
