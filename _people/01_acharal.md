---
layout: post
title: Angelos Charalambidis
permalink: /about/people/acharal.html
img: assets/faces/acharal.jpeg
tags: [people]
---

![photo](/assets/faces/acharal.jpeg)

Angelos Charalambidis holds a BSc in Informatics and
Telecommunications (University of Athens, 2005), an MSc in Computer
Systems (University of Athens, 2008) and a PhD on programming
languages and computational logic (University of Athens, 2014).
Angelos' main research interests are computational logic, knowledge
representation, the semantics and implementation of programming
languages and their various applications to data management. He has
been affiliated to NCSR-D since 2007 and participated in several
research Horizon 2020, FP7 and FP6 projects.

<a href="https://github.com/acharal" title="Follow him on GitHub" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-github fa-stack-1x"></i></span>
<a href="https://bitbucket.com/acharal" title="Follow him on Bitbucket" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-bitbucket fa-stack-1x"></i></span>
<a href="https://gitlab.com/acharal" title="Follow him on GitLab" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-gitlab fa-stack-1x"></i></span>
<a href="https://linkedin.com/in/acharal" title="Follow him on LinkedIn" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-linkedin fa-stack-1x"></i></span></a>
