---
layout: post
title: Giannos Chatziagapis
permalink: /about/people/giannos.html
img: assets/faces/giannos.jpeg
---

![photo](/assets/faces/giannos.jpeg)

Giannos Chatziagapis holds a BSc in Informatics and Telecommunications
(University of Athens, 2014). He is currently studying for a Master's
degree in Algorithms, Logic and Discrete Mathematics (University of
Athens).

<a href="https://github.com/giannos-ch" title="Follow him on GitHub" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-github fa-stack-1x"></i></span>
<a href="https://bitbucket.com/giannos_ch" title="Follow him on Bitbucket" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-bitbucket fa-stack-1x"></i></span>
<a href="https://gitlab.com/giannos_ch" title="Follow him on GitLab" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-gitlab fa-stack-1x"></i></span>
<a href="https://linkedin.com/in/giannos-chatziagapis" title="Follow him on LinkedIn" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-linkedin fa-stack-1x"></i></span></a>
