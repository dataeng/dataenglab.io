---
layout: post
title: Former Members
permalink: /about/people/gone
img: "assets/faces/former.png"
---

<table>
<colgroup>
	<col width="30%" />
	<col width="70%" />
</colgroup>


<tr id="kzam">
<td markdown="span">
![image]({{ site.baseurl }}/assets/faces/kzam.jpeg)
**Katerina Zamani (2015-2017)**
<br/>
<a href="https://github.com/kzama" title="Follow her on GitHub" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-github fa-stack-1x"></i></span>
<a href="https://gr.linkedin.com/in/katerina-zamani-a9bb9784" title="Follow her on LinkedIn" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-linkedin fa-stack-1x"></i></span></a>
</td>
<td markdown="span">
Katerina Zamani holds a BSc in Informatics and Telecommunications, and
a MSc in Advanced Information Systems from the University of Athens.
Since March 2015 she was a research associate at NCSR-D, working on
Machine Learning, Pattern Recognition and Similarity Learning issues.
<br/><br/>
At the end of 2017 Katerina moved on to pursue a carrier in industry.
</td>
</tr>


</table>
