---
layout: post
title: Kelly Kostopoulou
permalink: /about/people/kelkost.html
img: assets/faces/kelkost.jpeg
tags: [people]
---

![photo](/assets/faces/kelkost.jpeg)

Kelly Kostopoulou holds a BSc in Informatics and
Telecommunications (University of Athens, 2005).

<a href="https://github.com/DelphianCalamity" title="Follow her on GitHub" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-github fa-stack-1x"></i></span>
<a href="https://gitlab.com/DelphianCalamity" title="Follow her on GitLab" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-gitlab fa-stack-1x"></i></span>
<a href="https://www.linkedin.com/in/calliope-kostopoulou" title="Follow her on LinkedIn" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-linkedin fa-stack-1x"></i></span></a>
