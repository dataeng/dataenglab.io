---
layout: post
title: Ioannis Mouchakis
permalink: /about/people/gmouchakis.html
img: assets/faces/empty.png
---

Ioannis Mouchakis holds a BSc in Computer Science from the Department
of Informatics of Athens University of Economics and Business, 2010.
Since 2010 Mr Mouchakis is a research associate at NCSR-D, where he
has worked on Research and Technology Development tasks in multiple
European and national research projects. His expertise and interests
include deployment, maintenance, and development for Big Data
architectures, applied to a variety to domains such as event
recognition, e-Governance, and agro-environmental modelling.

<a href="https://github.com/gmouchakis" title="Follow him on GitHub" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-github fa-stack-1x"></i></span>
<a href="https://bitbucket.com/gmouchakis" title="Follow him on Bitbucket" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-bitbucket fa-stack-1x"></i></span>
<a href="https://gitlab.com/gmouchakis" title="Follow him on GitLab" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-gitlab fa-stack-1x"></i></span>
