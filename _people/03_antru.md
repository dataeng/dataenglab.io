---
layout: post
title: Antonis Troumpoukis
permalink: /about/people/antru.html
img: assets/faces/antru.jpeg
tags: [people]
---

![photo](/assets/faces/antru.jpeg)

Antonis Troumpoukis holds a BSc in Informatics and Telecommunications
(University of Athens, 2009), a MSc in Theoretical Computer Science
(University of Athens, 2011) and a PhD on programming languages and
knowledge representation (University of Athens, 2019). Antonis' main
research interests are knowledge representation, the semantics and
implementation of programming languages and their various applications
to data management. He has been affiliated to NCSR-D since 2015 and
participated in several research projects.

<a href="https://github.com/antru6" title="Follow him on GitHub" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-github fa-stack-1x"></i></span>
<a href="https://bitbucket.com/antru" title="Follow him on Bitbucket" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-bitbucket fa-stack-1x"></i></span>
<a href="https://linkedin.com/in/antonis-troumpoukis-a8755864/" title="Follow him on LinkedIn" target="_blank">
  <span class="fa-stack fa-lg"><i class="fa fa-linkedin fa-stack-1x"></i></span></a>

